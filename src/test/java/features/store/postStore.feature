@smoke
Feature: POST

  Scenario: Post Store endpoint
    Given I perform Post operation for "/store/order" with body
      | petId | quantity | status | complete |
      | 1     | 3        | placed | true     |
    When The response status code should be "200"
    Then The response is not empty
    And The response should be contains store "petId" like "1"

