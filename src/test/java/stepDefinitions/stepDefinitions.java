package stepDefinitions;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.response.Response;
import org.junit.Assert;
import utilities.StoreAPI;
import utilities.UserAPI;


import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class stepDefinitions {

    private static Response response;

    @Given("^I perform GET operation for \"([^\"]*)\"$")
    public void iPerformGETOperationFor(String url) throws URISyntaxException {
        StoreAPI storeAPI = new StoreAPI();
        response = storeAPI.GetStoreInventory(url);
    }

    @And("The response status code should be \"([^\"]*)\"$")
    public void theResponseStatusCodeShouldBe(int statusCode) {
        assertThat(response.getStatusCode(), equalTo(statusCode));
    }

    @Then("^The response should be contains store \"([^\"]*)\" like \"([^\"]*)\"$")
    public void theResponseShouldBeContainsStoreLike(String key, String value) {
        String storeValue = response.jsonPath().getString(key);
        Assert.assertEquals("The response not contains the store required.", value, storeValue);
    }

    @And("^The response (is|is not) empty$")
    public void theResponseEmpty(String value) {
        if (value.contains("is")) {
            assertThat(response.getBody().asString(), notNullValue());
        } else {
            assertThat(response.getBody().asString(), nullValue());
        }
    }

    @Given("^I perform Post operation for \"([^\"]*)\" with body$")
    public void iPerformPostOperationFor(String url, DataTable dataTable) throws URISyntaxException {
        StoreAPI storeAPI = new StoreAPI();
        List<List<String>> data = dataTable.raw();
        response = storeAPI.PostStore(data, url);
    }

    @Given("^I perform user Post operation for \"([^\"]*)\" with body$")
    public void iPerformUserPostOperationForWithBody(String url, DataTable dataTable) throws URISyntaxException {
        UserAPI userAPI = new UserAPI();
        List<List<String>> data = dataTable.raw();
        response = userAPI.PostUser(data, url);
    }

    @Given("^I perform user GET operation for \"([^\"]*)\"$")
    public void iPerformUserGETOperationFor(String url, DataTable dataTable) throws URISyntaxException {
        UserAPI userAPI = new UserAPI();
        List<List<String>> data = dataTable.raw();

        HashMap<String, String> pathParams = new HashMap<>();
        pathParams.put("username", data.get(1).get(0));

        response = userAPI.getByUsername(url, pathParams);

    }
}
